/*
Navicat MySQL Data Transfer

Source Server         : wyq
Source Server Version : 50627
Source Host           : localhost:3306
Source Database       : secondhand

Target Server Type    : MYSQL
Target Server Version : 50627
File Encoding         : 65001

Date: 2020-12-14 22:59:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `u_Account` varchar(100) NOT NULL,
  `u_Name` varchar(100) DEFAULT NULL,
  `u_Password` varchar(100) DEFAULT NULL,
  `u_Sex` varchar(10) DEFAULT NULL,
  `u_Email` varchar(100) DEFAULT NULL,
  `u_Phone` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`u_Account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1001', '小明', 'qweasd123', 'nan', '123456@qq.com', '12345678910');
INSERT INTO `user` VALUES ('1002', '小红', 'qweasd123', '男', '123456@qq.com', '12345678910');
INSERT INTO `user` VALUES ('1003', '小蓝', 'qweasd123', '男', '123456@qq.com', '12345678910');
INSERT INTO `user` VALUES ('1004', '小绿', 'qweasd123', '男', '123456@qq.com', '12345678910');
INSERT INTO `user` VALUES ('1005', '小紫', 'qweasd123', '男', '123456@qq.com', '12345678910');
INSERT INTO `user` VALUES ('1006', '小黄', 'qweasd123', '男', '123456@qq.com', '12345678910');
INSERT INTO `user` VALUES ('1007', '小橙', 'qweasd123', '男', '123456@qq.com', '12345678910');
INSERT INTO `user` VALUES ('1008', '大明', 'qweasd123', '男', '123456@qq.com', '12345678910');
INSERT INTO `user` VALUES ('1009', '张三', 'qweasd123', '男', '123456@qq.com', '12345678910');
INSERT INTO `user` VALUES ('1010', '李四', 'qweasd123', '男', '123456@qq.com', '12345678910');
INSERT INTO `user` VALUES ('1011', '巴乐水', 'qweasd123', '男', '123456@qq.com', '12345678910');
INSERT INTO `user` VALUES ('1012', '尤白梅', 'qweasd123', '男', '123456@qq.com', '12345678910');
INSERT INTO `user` VALUES ('1013', '祭致', 'qweasd123', '男', '123456@qq.com', '12345678910');
INSERT INTO `user` VALUES ('1014', '玉凡', 'qweasd123', '男', '123456@qq.com', '12345678910');
INSERT INTO `user` VALUES ('1015', '宇子', 'qweasd123', '男', '123456@qq.com', '12345678910');
INSERT INTO `user` VALUES ('admin', '洛水', 'qweasd123', null, null, null);
